
# Setup

```bash
# install vundle and clone the repository
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
git clone https://xemoe@bitbucket.org/xemoe/vimrc.git ~/dotfiles

echo 'source ~/dotfiles/vimrc.vim' > ~/.vimrc
echo 'source ~/dotfiles/autocomplete.vim' >> ~/.vimrc
cd ~/.vim/bundle/vimproc.vim/ && make
```
