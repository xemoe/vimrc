call plug#begin()

set completeopt=noinsert,menuone,noselect

Plug 'ncm2/ncm2-bufword'
Plug 'ncm2/ncm2-path'
Plug 'lvht/phpcd.vim', { 'for': 'php', 'do': 'composer install' }
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'c9s/phpunit.vim'
Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'easymotion/vim-easymotion'
Plug 'StanAngeloff/php.vim'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'tpope/vim-sleuth'
Plug 'vim-vdebug/vdebug'
Plug 'challenger-deep-theme/vim', { 'as': 'challenger-deep' }
Plug 'janko-m/vim-test'
Plug 'vim-airline/vim-airline'
Plug 'majutsushi/tagbar'

"
" Vim PHP
"
Plug 'autozimu/LanguageClient-neovim', { 'do': ':UpdateRemotePlugins' }
Plug 'junegunn/fzf'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'Shougo/echodoc.vim'

Plug 'stamblerre/gocode', { 'rtp': 'nvim', 'do': '~/.config/nvim/plugged/gocode/nvim/symlink.sh' }

"
" Vim Go
"
Plug 'fatih/vim-go'
Plug 'godoctor/godoctor.vim'
Plug 'zchee/deoplete-go', {'build': {'unix': 'make'}}
Plug 'jodosha/vim-godebug'

call plug#end()

"" Maps additional php extensions
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
if has("autocmd")
  augroup module
    autocmd BufRead,BufNewFile *.module set filetype=php
    autocmd BufRead,BufNewFile *.install set filetype=php
    autocmd BufRead,BufNewFile *.test set filetype=php
    autocmd BufRead,BufNewFile *.inc set filetype=php
    autocmd BufRead,BufNewFile *.profile set filetype=php
    autocmd BufRead,BufNewFile *.view set filetype=php
  augroup END
endif

"" Show line numbers.
set number
set relativenumber
filetype indent on

"" Reloads the vim config after saving.
augroup myvimrc
        au!
        au BufWritePost .vimrc,_vimrc,vimrc,.gvimrc,_gvimrc,gvimrc so $MYVIMRC | if has('gui_running') | so $MYGVIMRC | endif
augroup END

inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

"" Nerd tree
let g:nerdtree_tabs_open_on_console_startup=0
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
""" Key: CTRL+n         | Toggle tree
map <C-n> :NERDTreeToggle<CR>

"" TestSuite
" Runner
let test#strategy = "neovim"
let test#php#phpunit#executable = 'vendor/bin/phpunit'
""" Key: $mod+t         | Test nearest
""" Key: $mod+T         | Test File
""" Key: $mod+a         | Test suite
""" Key: $mod+l         | Test Test last
""" Key: $mod+g         | Test Visit
nmap <silent> <leader>t :TestNearest<CR>
nmap <silent> <leader>T :TestFile<CR>
nmap <silent> <leader>a :TestSuite<CR>
nmap <silent> <leader>l :TestLast<CR>
nmap <silent> <leader>g :TestVisit<CR>
" Maps .feature extension to cucumber

"" Php autocomplete
let g:LanguageClient_diagnosticsDisplay = {}
set hidden
let g:deoplete#enable_at_startup = 1
""" Key: K                | Action: Hover
""" Key: gd               | Action: go to definition
""" Key: f2               | Action: Rename

let g:deoplete#ignore_sources = get(g:, 'deoplete#ignore_sources', {})
let g:deoplete#ignore_sources.php = ['omni']

"" Git gutter.
set signcolumn=yes

"" Visual settings
if has('nvim')
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif

set encoding=utf-8
set colorcolumn=120
syntax on

"" Clipboard
set clipboard=unnamedplus

let g:phpcd_php_cli_executable = 'php7.2'

" hi CursorLine   cterm=NONE ctermbg=darkred ctermfg=white
hi CursorLine   cterm=NONE ctermbg=52 ctermfg=NONE
" hi CursorLine   cterm=NONE ctermbg=17 ctermfg=NONE
" hi CursorLine   cterm=NONE ctermbg=254 ctermfg=NONE

set background=dark
