set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'scrooloose/nerdTree'
Plugin 'elzr/vim-json'
Plugin 'ekalinin/Dockerfile.vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'zhaocai/GoldenView.Vim'
" Plugin 'fatih/vim-go'
" Plugin 'wakatime/vim-wakatime'

"
" use command source % and
" :PluginInstall
" to update vundle plugins
"

call vundle#end()

"
" vim-airline theme
"
let g:airline_theme='simple'

"
" vim-indent-guides
"
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=red   ctermbg=3
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=green ctermbg=4

"
" vim-easymotion
"
let g:EasyMotion_do_mapping = 0
let g:EasyMotion_smartcase = 1
nmap s <Plug>(easymotion-overwin-f)
nmap s <Plug>(easymotion-overwin-f2)

" JK motions: Line motions
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)

"
" GoldenView.Vim
"
let g:goldenview__enable_at_startup = 1


"
" common vimrc setup
"
filetype plugin indent on
set history=1000
set autoread
syntax on
set number
set laststatus=2
set noswapfile
set nobackup
set ts=4 sw=4 et
set ruler
set showcmd
set wildmenu
set incsearch
set hlsearch
set cursorline
set encoding=utf8
set ffs=unix,mac,dos
set t_Co=256
set backspace=indent,eol,start

highlight clear SignColumn
highlight clear LineNr

"
" let's vim to yank and paste outside
"
set clipboard=unnamed

"
" Custom syntax for filetypes
"
au FileType yaml setl sw=2 sts=2 et
